<?php

/**
 * @file
 * Contains role_watchdog.module.
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\role_watchdog\Entity\RoleWatchdog;

/**
 * Implements hook_help().
 */
function role_watchdog_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the role_watchdog module.
    case 'help.page.role_watchdog':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Role watchdog will automatically start recording all role changes. No further configuration is necessary for this functionality, the module will do this "out of the box". A record of these changes is shown in a Role history tabs on each user\'s page.') . '</p>' .
      '<p>' . t('Role watchdog can optionally email members of selected Notify roles when selected Monitor roles are added or removed. This was specifically added to keep a closer eye on certain role changes, such as an Administrator role. At least one Monitor role and one Notify role must be selected for this functionality.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_ENTITY_TYPE_update().
 */
function role_watchdog_user_update(EntityInterface $account) {
  $uid = \Drupal::currentUser()->id();
  $user_performed_on = $account->id();

  $roles_added = array_diff($account->getRoles(), $account->original->getRoles());
  $config = \Drupal::config('role_watchdog.settings');
  if (!empty($roles_added)) {
    role_watchdog_save_entity('Roles added', $uid,
      RoleWatchdog::ROLE_ADDED, $user_performed_on, $roles_added);
    if ($config->get('role_watchdog_notify_email') != '') {
      role_watchdog_notify('Roles Added', $roles_added, $user_performed_on);
    }
    if ($config->get('role_watchdog_use_watchdog')) {
      role_watchdog_log_change('Roles Added', $roles_added, $user_performed_on);
    }
  }

  $roles_removed = array_diff($account->original->getRoles(), $account->getRoles());
  if (!empty($roles_removed)) {
    role_watchdog_save_entity('Roles removed', $uid,
      RoleWatchdog::ROLE_REMOVED, $user_performed_on, $roles_removed);
    if ($config->get('role_watchdog_notify_email') != '') {
      role_watchdog_notify('Roles Removed', $roles_removed, $user_performed_on);
    }
    if ($config->get('role_watchdog_use_watchdog')) {
      role_watchdog_log_change('Roles Removed', $roles_removed, $user_performed_on);
    }
  }
}

/**
 * Implements hook_ENTITY_TYPE_insert().
 */
function role_watchdog_user_insert(EntityInterface $account) {
  $uid = \Drupal::currentUser()->id();

  role_watchdog_save_entity('Roles added', $uid,
    RoleWatchdog::ROLE_ADDED, $account->id(), $account->getRoles());
  if (\Drupal::config('role_watchdog.settings')->get('role_watchdog_notify_email') != '') {
    role_watchdog_notify('Roles Added', $account->getRoles(), $account->id());
  }
}

/**
 * Helper function to save a RoleWatchdog entity.
 *
 * @param string $name
 *   The name of the entity.
 * @param int $uid
 *   The user ID associated with the entity.
 * @param string $action
 *   The action performed for the entity.
 * @param int $user_performed_on
 *   The user ID on whom the action was performed.
 * @param array $rid
 *   An array of role IDs associated with the entity.
 *
 * @throws \Drupal\Core\Entity\EntityStorageException
 *   Thrown if there's an issue saving the entity.
 */
function role_watchdog_save_entity($name, $uid, $action, $user_performed_on, array $rid) {
  $entity = RoleWatchdog::create([
    'type' => 'role_grants',
    'name' => $name,
    'uid' => $uid,
    'action' => $action,
    'field_user_performed_on' => $user_performed_on,
    'field_role_id' => $rid,
  ]);
  $entity->save();
}

/**
 * Implements hook_ENTITY_TYPE_delete().
 */
function role_watchdog_user_delete(EntityInterface $account) {
  // Deleting all the role watchdog entities created because of a
  // particular user.
  $query = \Drupal::entityQuery('role_watchdog')
    ->accessCheck(FALSE)
    ->condition('field_user_performed_on', $account->id());
  $ids = $query->execute();
  $storage_handler = \Drupal::entityTypeManager()->getStorage('role_watchdog');
  $entities = $storage_handler->loadMultiple($ids);
  $storage_handler->delete($entities);
}

/**
 * Implements hook_mail().
 */
function role_watchdog_mail($key, &$message, $params) {
  $options = [
    'langcode' => $message['langcode'],
  ];

  switch ($key) {
    case 'notification':
      $message['from'] = \Drupal::config('system.site')->get('mail');
      $message['subject'] = t('Role watchdog notification on @site.', [
        '@site' => \Drupal::config('system.site')->get(
        'name'),
      ], $options);
      $message['body'][] = $params['message'];
      break;
  }
}

/**
 * Helper function to send a notification email for role changes.
 *
 * @param string $role_message
 *   A message describing the role change.
 * @param array $roles_changed
 *   An array of roles that were changed.
 * @param int $account_id
 *   The user account ID for whom the roles were changed.
 */
function role_watchdog_notify($role_message, $roles_changed, $account_id) {
  $role_watchdog_config = \Drupal::config('role_watchdog.settings');
  $mail_manager = \Drupal::service('plugin.manager.mail');
  $langcode = \Drupal::currentUser()->getPreferredLangcode();
  $params['message'] = t('Roles changed for User ID @account_id. @role_message : @roles_changed', [
    '@account_id' => $account_id,
    '@role_message' => $role_message,
    '@roles_changed' => implode(',', $roles_changed),
  ]);
  $notify = $mail_manager->mail('role_watchdog', 'notification', $role_watchdog_config->get('role_watchdog_notify_email'), $langcode, $params, NULL, 'TRUE');
  if ($notify['result'] !== TRUE) {
    \Drupal::logger('role_watchdog')->warning('Error sending notification email for Role Watchdog.');
  }
  else {
    \Drupal::logger('role_watchdog')->notice('Email notification for Role Watchdog sent.');
  }

}

/**
 * Helper function to write logging information to the Drupal PSR-3 logger.
 *
 * @param string $role_message
 *   Role message. Either "Roles Added" or "Roles Removed".
 * @param array $roles_changed
 *   The list of roles that were added or removed.
 * @param int $account_id
 *   The account that was changed.
 */
function role_watchdog_log_change($role_message, array $roles_changed, $account_id) {
  \Drupal::logger('role_watchdog')
    ->notice('Role changed for User ID @account_id. @role_message : @roles_changed', [
      '@account_id' => $account_id,
      '@role_message' => $role_message,
      '@roles_changed' => implode(',', $roles_changed),
    ]);
}
