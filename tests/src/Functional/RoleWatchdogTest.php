<?php

namespace Drupal\Tests\role_watchdog\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests to ensure working of Role Watchdog modules.
 *
 * @group role_watchdog
 */
class RoleWatchdogTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['role_watchdog'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->user = $this->drupalCreateUser(['administer role_watchdog']);
    $this->drupalLogin($this->user);
  }

  /**
   * Tests that the home page loads with a 200 response.
   */
  public function testLoad() {
    $this->drupalGet(Url::fromRoute('<front>'));
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Tests that the settings form provided by module works.
   */
  public function testSettingsPageLoad() {
    $this->drupalGet(Url::fromRoute('role_watchdog.role_watchdog_settings_form'));
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Tests working of view pages for user with appropriate permission.
   */
  public function testViewPagesLoad() {
    $authenticated_user = $this->createUser([
      'access role_watchdog reports',
    ]);
    $this->drupalLogin($authenticated_user);
    $uri = 'internal:/user/' . $authenticated_user->id() . '/track-grants';
    $this->drupalGet(Url::fromUri($uri));
    $this->assertSession()->statusCodeEquals(200);
  }

}
