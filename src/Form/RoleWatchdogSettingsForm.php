<?php

namespace Drupal\role_watchdog\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;

/**
 * Defines the RoleWatchdogSettingsForm class.
 *
 * Provides a configuration form for Role Watchdog settings, allowing users to
 * specify roles to monitor for changes and set up email notifications.
 */
class RoleWatchdogSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'role_watchdog.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'role_watchdog_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('role_watchdog.settings');
    $form['role_watchdog_use_watchdog'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log changes in user role to watchdog?'),
      '#default_value' => $config->get('role_watchdog_use_watchdog'),
    ];
    $form['email'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Email Settings'),
      '#description' => $this->t('Role watchdog can monitor one or more roles for changes and notify members of selected roles via email whenever a change occurs. At least one Monitor and one Notify role must be selected for this functionality.'),
    ];
    $roles = Role::loadMultiple();
    unset($roles[RoleInterface::ANONYMOUS_ID]);
    foreach ($roles as $key => $value) {
      $roles_options[$key] = $value->get('label');
    }
    $form['email']['role_watchdog_monitor_roles'] = [
      '#type' => 'select',
      '#title' => $this->t('Monitor for change'),
      '#options' => $roles_options,
      '#default_value' => $config->get('role_watchdog_monitor_roles') != NULL ? $config->get('role_watchdog_monitor_roles') : NULL,
      '#description' => $this->t('Select roles to monitor for change.'),
      '#multiple' => TRUE,
    ];
    $form['email']['role_watchdog_notify_email'] = [
      '#type' => 'email',
      '#title' => $this->t('Notify email on role change.'),
      '#default_value' => $config->get('role_watchdog_notify_email') != NULL ? $config->get('role_watchdog_notify_email') : NULL,
      '#description' => $this->t("Email address to notify on change. Leave it blank in case notifications aren't needed."),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('role_watchdog.settings')
      ->set('role_watchdog_monitor_roles', $form_state->getValue('role_watchdog_monitor_roles'))
      ->set('role_watchdog_notify_email', $form_state->getValue('role_watchdog_notify_email'))
      ->set('role_watchdog_use_watchdog', $form_state->getValue('role_watchdog_use_watchdog'))
      ->save();
  }

}
