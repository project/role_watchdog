<?php

namespace Drupal\role_watchdog\Plugin\migrate\source\d6;

use Drupal\migrate\Plugin\migrate\source\SqlBase;
use Drupal\migrate\Row;

/**
 * Drupal 6 role watchdog source from database.
 *
 * @MigrateSource(
 *   id = "d6_role_watchdog",
 *   source_module = "role_watchdog"
 * )
 */
class RoleWatchdog extends SqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('role_watchdog', 'rw')
      ->fields('rw', ['aid', 'action', 'uid', 'stamp'])
      ->distinct();

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $aid = $row->getSourceProperty('aid');
    $action = $row->getSourceProperty('action');
    $uid = $row->getSourceProperty('uid');
    $stamp = $row->getSourceProperty('stamp');

    $query = $this->select('role_watchdog', 'rw')
      ->fields('rw', ['rid']);
    $query->condition('aid', $aid);
    $query->condition('action', $action);
    $query->condition('uid', $uid);
    $query->condition('stamp', $stamp);
    $result = $query->execute()->fetchCol();

    // Prepare associated array for migration subprocess plugin.
    $rid = [];
    foreach ($result as $value) {
      $rid[] = [
        'value' => $value,
      ];
    }

    $row->setSourceProperty('rid', $rid);

    return parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'hid' => $this->t('History ID'),
      'aid' => $this->t('User ID of account'),
      'rid' => $this->t('Role ID changed'),
      'action' => $this->t('Action (add or remove) performed'),
      'uid' => $this->t('User ID performing action'),
      'stamp' => $this->t('Time action performed'),
    ];

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'stamp' => [
        'type' => 'integer',
        'alias' => 'rw',
      ],
      'aid' => [
        'type' => 'integer',
        'alias' => 'rw',
      ],
      'action' => [
        'type' => 'integer',
        'alias' => 'rw',
      ],
      'uid' => [
        'type' => 'integer',
        'alias' => 'rw',
      ],
    ];
  }

}
